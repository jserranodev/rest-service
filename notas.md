# "Building a RESTful Web Service"

## Notas

- Clase representación recurso
  - clase con:
    - attributos
    - constructor nulo
    - constructor con atributos
    - Getters y Setters
    - (opcional) método toString().
  - o POJO (Plain Old Java Object)
- [Jackson](https://github.com/FasterXML/jackson)
  - Biblioteca más usada pasar objetos JSON a objetos Java.
  - Biblioteca usada por Spring.
- Anotación `@RestController`
  - Identifica clase gestiona peticiones HTTP en REST web service.
  - Atajo de usar anotaciones `@Controller` y `@ResponseBody`.
  - Devuelve objeto JSON en vez de una vista.
- Anotación `@GetMapping("/path")`
  - Une el verbo GET y la ruta "/path" con método de clase con `@RestController`.
  - Forma generica asociar verbo: `@RequestMapping(method=VERBO_HTTP)`
- Anotación `@RequestParam()`
  - Asocia parametros petición HTTP con parámetros método clase Controller.
  - Ej.: `greeting(@RequestParam(value = "name", defaultValue = "World") String name){ ... }`
    - Asocia parametro HTTP name con parámetro método name
- Anotación `@SpringBootApplication`
  - Atajo de las anotaciones:
    - `@Configuration` establece clase principal, fuente de definiciones de Beans para Contexto de Aplicación (Application Context).
    - `@EnableAutoConfiguration` indica a SpringBoot empiece añadir Beans usando como base ajustes del classpath, otras Beansy ajustes de propiedades. Ej.: si spring-webmc esta se marca la app com webapp y activa comportamientos clave de webapp.
    - `@ComponentScan` dice a Spring busque componentes, configuraciones y servicios en /com/example. Permite encontrar los Controllers.
- Ejecutar y compilar JAR
  - Maven
    - Ejecutar: `./mvnw spring-boot:run`
    - Compilar: `./mvnw clean package` (limpiar y después empaquetar).
      - Ejecutar compilación: `java -jar target/nombre-binarion.jar`.

## Fuentes

- [Enlace](https://spring.io/guides/gs/rest-service/) a guía